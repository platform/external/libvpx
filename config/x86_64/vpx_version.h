// This file is generated. Do not edit.
#ifndef VPX_VERSION_H_
#define VPX_VERSION_H_
#define VERSION_MAJOR  1
#define VERSION_MINOR  15
#define VERSION_PATCH  0
#define VERSION_EXTRA  "1679-ge326c38abc"
#define VERSION_PACKED ((VERSION_MAJOR<<16)|(VERSION_MINOR<<8)|(VERSION_PATCH))
#define VERSION_STRING_NOSP "v1.15.0-1679-ge326c38abc"
#define VERSION_STRING      " v1.15.0-1679-ge326c38abc"
#endif  // VPX_VERSION_H_
